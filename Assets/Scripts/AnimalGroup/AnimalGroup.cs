using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class AnimalGroup : IAnimalGroup
{
    readonly Settings settings;
    private int animalsCount = 0;
    public AnimalGroup(Settings settings)
    {
        this.settings = settings;
    }

    public bool AddAnimal(out int followingIndex)
    {
        followingIndex = animalsCount;
        if (animalsCount < settings.maxAnimalCount)
        {
            animalsCount++;
            return true;
        }
        return false;
    }

    public void RemoveAnimal()
    {
        animalsCount--;
    }

    [Serializable]
    public class Settings
    {
        public int maxAnimalCount;
    }
}