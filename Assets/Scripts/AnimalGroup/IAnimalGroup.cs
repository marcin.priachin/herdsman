public interface IAnimalGroup
{
    bool AddAnimal(out int followingIndex);
    void RemoveAnimal();
}
