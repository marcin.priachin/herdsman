using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Player : ITickable, IPlayer
{
    readonly Settings settings;
    private Vector2 targetPosition;
    private Transform transform;

    public Player(Settings settings, Transform transform)
    {
        this.settings = settings;
        this.transform = transform;
        targetPosition = transform.position;
    }

    public void Tick()
    {
        Move();
    }

    private void Move()
    {
        if (Input.GetMouseButton(0))
        {
            targetPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition); 
        }

        if (Vector3.Distance(transform.position, targetPosition) > 0.01f)
        {
            transform.up = (Vector3)targetPosition - transform.position;
        }

        transform.position = Vector3.MoveTowards(transform.position, targetPosition, settings.speed * Time.deltaTime);
    }

    public Vector3 AnimalPosition(int animalIndex)
    {
        return transform.TransformPoint(new Vector3(0, -animalIndex - 1));
    }

    [Serializable]
    public class Settings
    {
        public float speed;
    }
}
