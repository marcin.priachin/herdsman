using UnityEngine;

public interface IPlayer
{
    Vector3 AnimalPosition(int animalIndex);
}
