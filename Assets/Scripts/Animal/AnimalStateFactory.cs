using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ModestTree;
using Zenject;

public enum AnimalStates
{
    Roaming,
    Following,
    Herded
}

public class AnimalStateFactory
{
    readonly AnimalStateRoaming.Factory roamingFactory;
    readonly AnimalStateFollowing.Factory followingFactory;
    readonly AnimalStateHerded.Factory herdedFactory;

    public AnimalStateFactory(
    AnimalStateRoaming.Factory roamingFactory,
    AnimalStateFollowing.Factory followingFactory,
    AnimalStateHerded.Factory herdedFactory)
    {
        this.roamingFactory = roamingFactory;
        this.followingFactory = followingFactory;
        this.herdedFactory = herdedFactory;
    }

    public AnimalState CreateState(AnimalStates state)
    {
        switch (state)
        {
            case AnimalStates.Roaming:
            {
                return roamingFactory.Create();
            }
            case AnimalStates.Following:
            {
                return followingFactory.Create();
            }
            case AnimalStates.Herded:
            {
                return herdedFactory.Create();
            }
        }

        throw Assert.CreateException();
    }
}
