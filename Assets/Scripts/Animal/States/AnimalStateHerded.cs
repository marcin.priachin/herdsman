using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class AnimalStateHerded : AnimalState
{
    [Inject] readonly IAnimalGroup animalGroup;
    [Inject] readonly IGUIManager gUIManager;
    readonly Animal animal;

    public AnimalStateHerded(Animal animal)
    {
        this.animal = animal;
    }

    public override void Start()
    {
        animalGroup.RemoveAnimal();
    }

    public override void Update()
    {
        animal.transform.position = Vector3.MoveTowards(animal.transform.position, animal.AnimalData.FinishPosition, Time.deltaTime * animal.AnimalData.AnimalSettings.speed);

        if(Vector3.Distance(animal.transform.position, animal.AnimalData.FinishPosition) == 0)
        {
            gUIManager.UpdateScore();
            animal.Dispose();
        }
    }

    public class Factory : PlaceholderFactory<AnimalStateHerded>
    {

    }
}
