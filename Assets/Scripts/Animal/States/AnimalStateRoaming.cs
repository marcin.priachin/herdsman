using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class AnimalStateRoaming : AnimalState
{
    [Inject] readonly IAnimalGroup animalGroup;
    readonly Animal animal;

    public AnimalStateRoaming(Animal animal)
    {
        this.animal = animal;
    }

    public override void Update()
    {

    }

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player") && animalGroup.AddAnimal(out int followingIndex))
        {
            animal.AnimalData.FollowingIndex = followingIndex;
            animal.ChangeState(AnimalStates.Following);
        }
    }

    public class Factory : PlaceholderFactory<AnimalStateRoaming>
    {

    }
}
