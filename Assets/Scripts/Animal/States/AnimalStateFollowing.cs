using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class AnimalStateFollowing : AnimalState
{
    [Inject] readonly IPlayer player;
    readonly Animal animal;

    public AnimalStateFollowing(Animal animal)
    {
        this.animal = animal;
    }

    public override void Start()
    {

    }

    public override void Update()
    {
        Vector3 animalTarget = player.AnimalPosition(animal.AnimalData.FollowingIndex);
        animal.transform.position = Vector3.MoveTowards(animal.transform.position, animalTarget, Time.deltaTime * animal.AnimalData.AnimalSettings.speed);
    }

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Finish"))
        {
            animal.AnimalData.FinishPosition = collision.transform.position;
            animal.ChangeState(AnimalStates.Herded);
        }
    }

    public class Factory : PlaceholderFactory<AnimalStateFollowing>
    {

    }
}
