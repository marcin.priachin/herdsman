using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AnimalState : IDisposable
{
    public abstract void Update();

    public virtual void Start() { }

    public virtual void Dispose() { }

    public virtual void OnTriggerEnter2D(Collider2D collision) { }
}
