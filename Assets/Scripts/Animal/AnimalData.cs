using System;
using UnityEngine;

public class AnimalData : IAnimalData
{
    public Settings AnimalSettings { get; private set; }
    public int FollowingIndex { get; set; }
    public Vector3 FinishPosition { get; set; }

    public AnimalData(Settings settings)
    {
        AnimalSettings = settings;
    }

    [Serializable]
    public class Settings
    {
        public float speed;
    }
}