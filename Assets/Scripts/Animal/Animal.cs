using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Animal : MonoBehaviour, IDisposable
{
    [Inject] readonly AnimalStateFactory stateFactory;
    [Inject] public IAnimalData AnimalData { get; }
    private AnimalState state;

    private void Start()
    {
        ChangeState(AnimalStates.Roaming);
    }

    private void Update()
    {
        state.Update();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        state.OnTriggerEnter2D(collision);
    }

    public void ChangeState(AnimalStates animalState)
    {
        if (state != null)
        {
            state.Dispose();
            state = null;
        }

        state = stateFactory.CreateState(animalState);
        state.Start();
    }

    public void Dispose()
    {
        Destroy(gameObject);
    }
}
