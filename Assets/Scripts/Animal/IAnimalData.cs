using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAnimalData
{
    public AnimalData.Settings AnimalSettings { get; }
    public int FollowingIndex { get; set; }
    public Vector3 FinishPosition { get; set; }
}
