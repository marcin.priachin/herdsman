using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIManager : IGUIManager
{
    readonly Text scoreText;
    private int score = 0;

    public GUIManager(GameObject scoreGameObject)
    {
        scoreText = scoreGameObject.GetComponent<Text>();
    }

    public void UpdateScore()
    {
        score++;
        scoreText.text = score.ToString();
    }
}
