using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class GameInstaller : MonoInstaller
{
    [SerializeField] private Transform playerTransform;
    [SerializeField] private GameObject scoreGameObject;

    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<Player>().AsSingle().WithArguments(playerTransform);
        Container.BindInterfacesAndSelfTo<AnimalGroup>().AsSingle();
        Container.BindInterfacesAndSelfTo<GUIManager>().AsSingle().WithArguments(scoreGameObject);
    }
}
