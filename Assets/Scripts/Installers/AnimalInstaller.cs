using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class AnimalInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<AnimalData>().AsSingle();
        Container.Bind<AnimalStateFactory>().AsSingle();
        Container.BindFactory<AnimalStateRoaming, AnimalStateRoaming.Factory>().WhenInjectedInto<AnimalStateFactory>();
        Container.BindFactory<AnimalStateFollowing, AnimalStateFollowing.Factory>().WhenInjectedInto<AnimalStateFactory>();
        Container.BindFactory<AnimalStateHerded, AnimalStateHerded.Factory>().WhenInjectedInto<AnimalStateFactory>();
    }
}
