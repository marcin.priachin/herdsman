using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

[CreateAssetMenu(menuName = "Custom/Settings")]
public class SettingsInstaller : ScriptableObjectInstaller<SettingsInstaller>
{
    public Player.Settings player;
    public AnimalData.Settings animalData;
    public AnimalGroup.Settings animalGroup;

    public override void InstallBindings()
    {
        Container.BindInstance(player);
        Container.BindInstance(animalData);
        Container.BindInstance(animalGroup);
    }
}
